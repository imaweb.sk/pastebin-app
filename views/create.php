<?php include("partials/header.php"); ?>

<main class="main-create">
	<div class="container">
		<h5 class="pt-4 pt-lg-5">
			<span>PASTEBIN: </span>
			<span class="font-weight-normal">Create new pastebin</span>
		</h5>
		<hr>
		<input
			type="text"
			id="main-create-link"
			value="<?php echo $_SERVER['HTTP_HOST'] . "/open?id=" . $uuid ?>"
		>
		<input 
			type="text" 
			id="uuid"
			name="uuid"
			value="<?php echo $uuid; ?>"
			hidden
		>
		<textarea
			name="text"
			id="text"
			cols="30"
			rows="5"
			placeholder="Insert the text here"
			autofocus
		></textarea>
		<button 
			class="main-create-generate"
			onclick="generate()"
			data-clipboard-action="copy"
			data-clipboard-target="#main-create-link"
		>
			<i class="fas fa-copy mr-2"></i>
			<span>Create new pastebin</span>
		</button>
	</div>
</main>

<?php include("partials/footer.php"); ?>