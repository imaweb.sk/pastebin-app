<?php include("partials/header.php"); ?>

<div class="main-generate">
	<img src="data:image/png;base64, <?php echo $qr_image; ?>" />
	<p><?php echo $secretKey; ?></p>
	<small>Scan QR code on your phone and add the secret key to credentials.json</small>
</div>

<?php include("partials/footer.php"); ?>