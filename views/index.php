<?php include("partials/header.php"); ?>

<main class="main-index">
	
	<div class="main-index-flex-holder">
		<div class="main-index-flex-child">
			<form 
				action="/create"
				method="post"
				class="main-index-login-form <?php echo $wrongCredentials; ?>"
				autocomplete="off" 
			>
				<input 
					type="text"
					name="password"
					placeholder="Insert password"
					autocomplete="off" 
					required
					autofocus 
				>
				<button 
					type="submit"
				>
					<i class="fas fa-check-circle"></i>
				</button>
			</form>
			<?php if($message) { ?>
				<div class='result-message'>
					<div class='message-icon'>
						<i class='far fa-check-circle'></i>
					</div>
					<div class='message-text'>
						<?php echo $message; ?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</main>

<?php include("partials/footer.php"); ?>