<?php 	
		
	use Ramsey\Uuid\Uuid;
	use PragmaRX\Google2FA\Google2FA;
	use BaconQrCode\Renderer\ImageRenderer;
	use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
	use BaconQrCode\Renderer\RendererStyle\RendererStyle;
	use BaconQrCode\Writer;

	class MainController {

		private $database;
		private $encryption;
		private $password;
		private $auth_name;
		private $auth_url;

		public function __construct($method)
		{
			$this->startSessions();
			$this->globalize();
			$this->deleteOldPastebins();
			$this->$method();
		}

		private function startSessions()
		{
			session_start();
		}

		private function globalize()
		{
			global $database;
			global $encryption;
			global $password;
			global $auth_name;
			global $auth_url;
			$this->database = $database;
			$this->encryption = $encryption;
			$this->password = $password;
			$this->name = $auth_name;
			$this->url = $auth_url;
		}

		private function generate()
		{
			if(!strlen($this->password) > 0) {
				$google2fa = new Google2FA();
				$secretKey = $google2fa->generateSecretKey();
				$qrCodeUrl = $google2fa->getQRCodeUrl(
				    $this->name,
				    $this->url,
				    $secretKey
				);
				$renderer = new ImageRenderer(
				    new RendererStyle(400),
				    new ImagickImageBackEnd()
				);
				$writer = new Writer($renderer);
				$qr_image = base64_encode($writer->writeString($qrCodeUrl));
				require("../views/generate.php");
			} else {
				header("location:/");
			}
		}

		private function index()
		{
			$message = isset($_SESSION['message']) ? $_SESSION['message'] : null;
			$wrongCredentials = isset($_SESSION['wrong-credentials']) ? $_SESSION['wrong-credentials'] : null;
			if(isset($_SESSION['message'])) {
				unset($_SESSION['message']);
			}
			if(isset($_SESSION['wrong-credentials'])) {
				unset($_SESSION['wrong-credentials']);
			}
			require("../views/index.php");
		}

		private function create()
		{
			$google2fa = new Google2FA();
			if(
				isset($_POST['password']) 
				&& is_numeric($_POST['password'])
				&& strlen($_POST['password']) == 6
				&& strlen($this->password) > 0
				&& $google2fa->verifyKey($this->password, filter_var($_POST['password'], FILTER_SANITIZE_STRING))
			) {
				$uuid = Uuid::uuid4();
				$_SESSION['uuid'] = $uuid;
				require("../views/create.php");			
			} else {
				$_SESSION['wrong-credentials'] = "main-index-wrong-credentials";
				header("location:/");
			}
		}

		private function save()
		{
			if(isset($_SESSION['uuid'])) {
				$uuid = $_SESSION['uuid'];
				unset($_SESSION['uuid']);
				if(
					isset($_POST['uuid']) 
					&& $uuid == $_POST['uuid']
				){
					if($this->database->insert(
						'pastebin', 
						[
					    	'uuid' => openssl_encrypt(
					    		$_POST['uuid'],
					    		"AES-128-ECB",
					    		$this->password
					    	),
					    	'text' => openssl_encrypt(
					    		$_POST['text'],
					    		"AES-128-ECB",
					    		$this->password
					    	),
					    	'date' => date('Y-m-d')
						]
					)){
						$_SESSION['message'] = 'Pastebin has been created and its URL is stored in your clipboard!';
						$_SESSION['message'] .= '<a href=\'mailto:?subject=One-time link&body=Dear [RECEIVER],<br><br>your one-time private message can be accessed here: <a href="https://' . $_SERVER['HTTP_HOST'] . "/open?id=" . $uuid .  '">' . $_SERVER['HTTP_HOST'] . "/open?id=" . $uuid .  '</a><br><br>Kind regards,<br>[SENDER]\' class=\'message-send-it\'><i class="far fa-paper-plane mr-2"></i>Send it right away</a>';
						echo "success";

					} else {
						echo "Error occured while inserting data to the database";
					}
				} else {
					die("Forbidden - Unauthorized access");
				}
			} else {
				die("Forbidden - Unauthorized access");
			} 
		}

		private function open()
		{
			if(isset($_GET['id'])) {
				$message = $this->database->select(
					"pastebin", 
					"text", 
					[
						"uuid" => filter_var(openssl_encrypt(
							$_GET['id'], 
							"AES-128-ECB",
							$this->password
						), FILTER_SANITIZE_STRING)
					]
				);
				if(isset($message[0])) { 
					$message = htmlspecialchars(
						openssl_decrypt(
							$message[0], 
							"AES-128-ECB",
							$this->password
						)
					);
					$this->deleteOpenedPastebin(
						filter_var(
							openssl_encrypt(
								$_GET['id'], 
								"AES-128-ECB",
								$this->password
							),
							FILTER_SANITIZE_STRING
						)
					);
					require("../views/open.php");
				} else {
					header("location:/");
				}
			} else {
				header("location:/");
			}
		}

		private function deleteOpenedPastebin($uuid)
		{
			$this->database->delete(
				"pastebin", 
				[
					"AND" => [
						"uuid" => $uuid
					]
				]
			);
		}

		private function deleteOldPastebins()
		{
			$this->database->delete(
				"pastebin", 
				[
					"AND" => [
						"date[<]" => date('Y-m-d')
					]
				]
			);
		}

	} new MainController($method);

?>